## OrangeFox Recovery Device Tree for the Samsung Galaxy A02s (Snapdragon)

**Android 10 & 11**

Based on TWRP Device Tree
https://github.com/mohammad92/android_device_samsung_a02q

Using Kernel source:
https://github.com/mohammad92/android_kernel_samsung_a02q


## How-to compile
```sh
cd ~/OrangeFox_10/fox_10.0
source build/envsetup.sh
export ALLOW_MISSING_DEPENDENCIES=true 
export FOX_USE_TWRP_RECOVERY_IMAGE_BUILDER=1
export LC_ALL="C"
export OF_VANILLA_BUILD=1
lunch omni_a02q-eng && mka recoveryimage
```
